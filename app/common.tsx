import {Action} from 'redux';
import {Actions} from "./Actions/actions";

/** Интерфейс для action*/
export interface IActionType extends Action {
    type: string;
    payload?: any;
}

/** Интерфейс для состояний*/
export interface ICommonConditions {
    loading: boolean;
    modalAdd: boolean,
    modalDelete: boolean,
    modalEdit: boolean,
    loginStatus: boolean;
    orgId: number
}

/** Интерфейс для авторизации*/
export interface ILogin {
    login: string;
    password: string;
}

/** Интерфейс для оформления модального окна*/
export interface IModalLabels {
    confirmName: string,
    title: string,
    currentID: number,
}

/** Интерфейс для dispatch*/
export interface IDispatchProps {
    actions: Actions;
}

/** Интерфейс для mapStateToProps*/
export interface ICompanies {
    companiesRdc: ICompaniesItem[];
    loginRdc: ILogin;
}

/** Интерфейс для организаций*/
export interface ICompaniesItem {
    id: number;
    name: string;
    address: string;
    INN: string;
}

/** Интерфейс для отделов*/
export interface IDepartmentsItem {
    id?: number;
    id_organization?: number;
    name: string;
    phone: string;
}

/** Интерфейс для сотрудников*/
export interface IEmployeesItem {
    id?: number;
    id_division?: number;
    FIO: string;
    address: string;
    position: string;
}

/** Интерфейс для полей ввода модального окна*/
export  interface IInput {
    name : string,
    address? : string,
    INN? : string,
    phone? : string,
    position? : string,
    FIO? : string
}
