import * as React from 'react';
import {connect} from 'react-redux';
import {Dispatch} from 'redux';
import {IActionType, ICompanies, IDispatchProps, ILogin} from '../../common';
import {Actions} from '../../Actions/actions';

/** Интерфейс состояний авторизации*/
interface IStateProps {
    loginStatus: boolean;
    waitingForLogin: boolean;
}

/** Итоговый интерфейс для props */
type TProps = IStateProps & IDispatchProps;

/** Итоговый интерфейс для state */
type TState = ILogin;

/** Компонент отвечающий за авторизацию*/
class Login extends React.Component<TProps, TState> {
    /** Стейт компонента
     * @login (string) - имя пользователя
     * @password (string) - пароль*/
    state: ILogin = {
        login: '',
        password: ''
    };

    /** Заносит изменения значений полей input в state*/
    onChange = (e: React.SyntheticEvent<HTMLInputElement>) => {
        const {id, value} = e.currentTarget;
        /** НЕПОНЯТКА
         *  без явного указания as Pick<ILogin, 'login' | 'password'>);
         *  вылетала ошибка. Решение нашел тут - https://vk.cc/9tD7WN*/
        this.setState({
            [id]: value
        } as Pick<ILogin, 'login' | 'password'>);
    };

    /** Обработчик кнопки "Вход"
     * @e (SyntheticEvent) - оболочка события для полей ввода логина и пароля*/
    handleLogin = (e: React.SyntheticEvent<HTMLButtonElement>) => {
        e.preventDefault();
        this.props.actions.onLogin(this.state);
    };

    /** Обработчик кнопки "Выход"*/
    handleLogout = (e: React.SyntheticEvent<HTMLButtonElement>) => {
        e.preventDefault();
        this.props.actions.onLogout();
    };

    /** Шаблон страницы логина*/
    loginPage = () => {
        const {loginStatus} = this.props;

        if (!loginStatus) {
            /** Вход не выполнен*/
            return (
                <React.Fragment>
                <div>
                    <div className="vIndent5"></div>
                    <header className="center">
                        Для просмотра и редактирования БД необходимо авторизоваться
                    </header>
                    <div className="logPass">
                        <div>
                            <div className="vIndent2_5"></div>
                            <span>Имя пользователя</span><br/>
                            <input
                                id="login"
                                placeholder="login"
                                value={this.state.login}
                                onChange={this.onChange}
                            /><br/>
                            <div className="vIndent2_5"></div>
                            <span>Пароль</span><br/>
                            <input
                                id="password"
                                type="password"
                                placeholder="password"
                                value={this.state.password}
                                onChange={this.onChange}
                            /><br/>
                            <div className="vIndent2_5"></div>
                        </div>
                    </div>
                </div>
                    <button
                        className="btn-success loginBtn"
                        onClick={this.handleLogin}>
                        Вход
                    </button>
                </React.Fragment>
            )
        } else {
            /** Вход выполнен*/
            return (
                <div>
                    <br/><br/>
                    <header className="txtCenter">
                        Авторизация прошла успешно.
                        Для выхода из режима просмотра и редактирования БД нажмите на "Выход".
                    </header>
                    <br/>
                    <button
                        className="btn-success loginBtn"
                        onClick={this.handleLogout}>
                        Выход
                    </button>
                </div>
            )
        }
    };

    /** Отрисовка страницы логина*/
    render() {
        return (
            <div>
                {this.loginPage()}
            </div>
        )
    }
}

function mapStateToProps(state: ICompanies) {
    return state.loginRdc;
}

function mapDispatchToProps(dispatch: Dispatch<IActionType>): IDispatchProps {
    return {
        actions: new Actions(dispatch)
    };
}

const connectLogin = connect(mapStateToProps, mapDispatchToProps)(Login);

export {connectLogin as Login};
