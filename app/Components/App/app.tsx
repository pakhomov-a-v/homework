import * as React from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import {Companies} from '../Companies/companies';
import {Departments} from '../Departments/departments';
import {Employees} from '../Employees/employees'
import Header from '../Header/header'
import {Login} from '../Login/login';
import {Modal} from '../Modal/modal'
import './app.less';

import Main from '../Main/main';

/** Основной компонент приложения*/
export default class App extends React.Component {
    render() {
        return (
            <div className="basis">
                <div className="">
                    <br/><span className="hdr">БАЗА ДАННЫХ</span>
                    <span className="subHdr">Организации. Отделы. Сотрудники.</span><br/><br/>
                </div>

                <Modal/>

                <div>
                    <BrowserRouter>
                        <div>
                            <Header/>
                            <Switch>
                                <Route path="/" component={Main} exact/>
                                <Route path="/login" component={Login}/>
                                <Route path="/companies" component={Companies}/>
                                <Route path="/department/:companyId" component={Departments}/>
                                <Route path="/employee/:divisionId" component={Employees}/>
                            </Switch>
                        </div>
                    </BrowserRouter>
                </div>
            </div>
        );
    }
}
