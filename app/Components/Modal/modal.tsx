import * as React from 'react';
import {Fields} from '../Fields/fields';

/** Компонент модального окна*/
const Modal = (props: any) => {
    /** @isOpen (boolean) - флаг открытия/закрытия окна
     * @close (function) - закрытие модального окна
     * @confirm (function) - нажатие на кнопке подтверждения действия
     * @data (any) - данные для компонента Fields
     * @onChange (function) - изменение полей input
     * @title (function) - шапка модального окна
     * @confirmName (string) - название кнопки подтверждения действия */
    const {isOpen, close, confirm, data, onChange, title, confirmName} = props;

    /** Окно закрыто*/
    if (!isOpen) return null;

    /** Окно открыто*/
    return (
        <div className="modal bs-component">
            <div className="modal-dialog-centered">
                <div className="col-3"></div>
                <div className="modal-content col-6 basis">
                    <div className="modal-header">
                        <h4 className="modal-title">{title}</h4>
                        <button type="button"
                                className="close"
                                onClick={close}>
                            <span>&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        <Fields item={data} onChange={onChange}/>
                    </div>
                    <div className="modal-footer">
                        <button
                            type="button"
                            className="btn-danger loginBtn"
                            onClick={close}>
                            ЗАКРЫТЬ
                        </button>
                        <button
                            type="button"
                            className="btn-success loginBtn"
                            onClick={confirm}>
                            {confirmName}
                        </button>
                    </div>
                </div>
                <div className="col-3"></div>
            </div>
        </div>
    )

};

export {Modal};
