import * as React from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import {Dispatch} from 'redux';
import {
    IActionType,
    ICommonConditions,
    ICompanies,
    ICompaniesItem,
    IDispatchProps,
    IModalLabels
} from '../../common';
import {Actions} from '../../Actions/actions';

import {Modal} from '../Modal/modal'

/** Интерфейс массива организаций*/
interface IStateProps {
    companies: ICompaniesItem[];
}

/** Итоговый интерфейс для props */
type TProps = IStateProps & IDispatchProps & ICommonConditions;

/** Интерфейс для объекта-организации*/
interface IInput {
    name: string;
    address: string;
    INN: string;
}

/** Интерфейс объекта-организации*/
interface ICompanyItem {
    item: IInput;
}

/** Итоговый интерфейс для state */
type TState = ICompanyItem & IModalLabels;

/** Компонент отвечающий за компании*/
class Companies extends React.Component<TProps, TState> {
    /** Стейт компонента
     * @confirmName (string) - название кнопки подтверждения действия
     * в модальном окне
     * @title (string) - заголовок модального окна
     * @currentID (number) - ID текущей организации
     * @item {} - Объект с полями name, address, INN организации
     * @item.name (string) - название организации
     * @item.address (string) - адрес организации
     * @item.INN (number) - ИНН организации */
    state: TState = {
        confirmName: '',
        title: '',
        currentID: null,
        item: {
            name: '',
            address: '',
            INN: ''
        }
    };

    /** Получение списка организаций*/
    componentDidMount() {
        this.props.actions.getCompanies();
    };

    /** Очистка стейта*/
    cleanState = () => {
        this.setState({
            confirmName: '',
            title: '',
            currentID: null,
            item: {
                name: '',
                address: '',
                INN: ''
            }
        });
    };

    /** Изменение данных в модальном окне
     * @e (SyntheticEvent) - оболочка события для измнения полей input*/
    onChange = (e: React.SyntheticEvent<HTMLInputElement>) => {
        const target = e.currentTarget;
        if (target.id === 'INN') {
            const charCode = target.value.charCodeAt(target.value.length - 1);
            if ((charCode >= 48 && charCode <= 57) || charCode === 46 || charCode === 8) {
                this.setState({
                    item: {
                        ...this.state.item,
                        [target.id]: target.value
                    }
                });
            }
        } else {
            const target = e.currentTarget;
            this.setState({
                item: {
                    ...this.state.item,
                    [target.id]: target.value
                }
            });
        }
    };

    /** Поиск организации
     * @idValue (number) - значение ID искомой организации*/
    searchCompany(idValue: number): ICompaniesItem {
        const companies = this.props.companies;
        let item: ICompaniesItem;

        for (let i = 0; i < companies.length; i++) {
            if (companies[i].id === idValue) {
                item = companies[i];
                return item;
            }
        }
        return
    };

    /** Заглушка для запрета изменения полей input при удалении*/
    nothing = () => {
        console.log('Absolutely nothing is happening')
    };

    /** Обработчик кнопки "Добавить организацию" */
    handleAdd = () => {
        this.props.actions.switchAddModal();
        this.setState({
            confirmName: 'ДОБАВИТЬ',
            title: 'Добавление организации',
        });
    };

    /** Обработчик кнопки "РЕДАКТИРОВАТЬ"
     * @e (SyntheticEvent) - оболочка события для клика
     * на кнопке Edit модального окна*/
    handleEdit = (e: React.SyntheticEvent<HTMLElement>) => {
        this.props.actions.switchEditModal();
        const idValue = parseInt(e.currentTarget.dataset.id);
        const company = this.searchCompany(idValue);

        this.setState({
            confirmName: 'СОХРАНИТЬ',
            title: 'Редактирование организации',
            currentID: idValue,
            item: {
                name: company.name,
                address: company.address,
                INN: company.INN
            }
        });
    };

    /** Обработчик кнопки "УДАЛИТЬ"
     * @e (SyntheticEvent) - оболочка события для клика
     * на кнопке Delete модального окна*/
    handleDelete = (e: React.SyntheticEvent<HTMLElement>) => {
        this.props.actions.switchDeleteModal();
        const idValue = parseInt(e.currentTarget.dataset.id);
        const company = this.searchCompany(idValue);
        this.setState({
            confirmName: 'УДАЛИТЬ',
            title: 'Удаление организации',
            currentID: idValue,
            item: {
                name: company.name,
                address: company.address,
                INN: company.INN
            }
        });
    };

    /** Обработчик кнопки close модального окна Add*/
    handleCloseAdd = () => {
        this.props.actions.switchAddModal();
        this.cleanState();
    };

    /** Обработчик кнопки close модального окна Edit*/
    handleCloseEdit = () => {
        this.props.actions.switchEditModal();
        this.cleanState();
    };

    /** Обработчик кнопки close модального окна Delete*/
    handleCloseDelete = () => {
        this.props.actions.switchDeleteModal();
        this.cleanState();
    };

    /** Обработчик кнопки "Add company" модального окна Add */
    handleAddConfirm = () => {
        this.props.actions.addCompany({
            ...this.state.item,
            id: this.state.currentID,
        }, this.props.companies);
        this.cleanState();
    };

    /** Обработчик кнопки "Save changes" модального окна Edit*/
    handleEditConfirm = () => {
        this.props.actions.editCompany({
            ...this.state.item,
            id: this.state.currentID,
        }, this.props.companies);
        this.cleanState();
    };

    /** Обработчик кнопки "Delete" модального окна Delete*/
    handleDeleteConfirm = () => {
        this.props.actions.deleteCompany(this.state.currentID, this.props.companies);
        this.cleanState();
    };

    /** Блок организаций*/
    nonModal() {
        let {companies, loginStatus} = this.props;
        /** Вход выполнен */
        if (loginStatus) {
            /** Вход выполнен, организаций нет */
            if (companies.length === 0) {
                return (
                    <React.Fragment>
                        <div className="vIndent5"></div>
                        <div
                            className="btn-primary txtCenter loginBtn"
                            onClick={this.handleAdd}>
                            Добавить организацию
                        </div>
                    </React.Fragment>
                )
            }
            /** Вход выполнен, организации есть */
            else {
                let output: JSX.Element | JSX.Element[] | string = companies.map(item => {
                    return (
                        <React.Fragment>
                            <div className="vIndent2_5"></div>
                            <div key={item.id}>
                                <div>
                                    <div>
                                        <div className="lightBack btn-no-hover txtCenter loginBtn">Название
                                            организации {item.name}</div>
                                        <div
                                            className="lightBack btn-no-hover txtCenter loginBtn">Адрес {item.address}</div>
                                        <div
                                            className="lightBack btn-no-hover txtCenter loginBtn">ИНН {item.INN}</div>
                                    </div>
                                    <div className="row pads">
                                        <div
                                            className="lightBack btn-primary width33 txtCenter loginBtn"
                                            data-id={item.id}
                                            onClick={this.handleEdit}>
                                            РЕДАКТИРОВАТЬ
                                        </div>
                                        <div
                                            className="lightBack btn-primary width29 txtCenter loginBtn"
                                            data-id={item.id}
                                            onClick={this.handleDelete}
                                        >УДАЛИТЬ
                                        </div>
                                        <Link
                                            className="lightBack btn-primary width33 txtCenter loginBtn"
                                            to={`/department/${item.id}`}>
                                            К ОТДЕЛАМ
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        </React.Fragment>
                    );
                });
                return (
                    <React.Fragment>
                        <div className="vIndent5"></div>
                        <div
                            className="btn-primary txtCenter loginBtn"
                            onClick={this.handleAdd}>
                            Добавить организацию
                        </div>
                        {output}
                    </React.Fragment>
                );
            }
        }
        /** Вход не выполнен */
        else {
            return (
                <React.Fragment>
                    <div className="vIndent5"></div>
                    <div className="txtCenter">
                        Пожалуйста залогиньтесь
                    </div>
                </React.Fragment>
            )
        }
    };

    /** Модальные окна*/
    modal() {
        /** для добавления*/
        if (this.props.modalAdd) {
            return <Modal
                confirmName={this.state.confirmName}
                title={this.state.title}
                isOpen={this.props.modalAdd}
                close={this.handleCloseAdd}
                confirm={this.handleAddConfirm}
                onChange={this.onChange}
                data={this.state.item}/>
        }
        /** для редактирования*/
        if (this.props.modalEdit) {
            return <Modal
                confirmName={this.state.confirmName}
                title={this.state.title}
                isOpen={this.props.modalEdit}
                close={this.handleCloseEdit}
                confirm={this.handleEditConfirm}
                onChange={this.onChange}
                data={this.state.item}/>
        }
        /** для удаления*/
        if (this.props.modalDelete) {
            return <Modal
                confirmName={this.state.confirmName}
                title={this.state.title}
                isOpen={this.props.modalDelete}
                close={this.handleCloseDelete}
                confirm={this.handleDeleteConfirm}
                onChange={this.nothing}
                data={this.state.item}/>
        }
        return
    }

    /** Орисовка блока организаций и модальных окон*/
    render() {
        return (
            <React.Fragment>
                <div>{this.nonModal()}</div>
                <div>{this.modal()}</div>
            </React.Fragment>
        )
    }
}

function mapStateToProps(state: ICompanies) {
    return {...state.companiesRdc, ...state.loginRdc};
}

function mapDispatchToProps(dispatch: Dispatch<IActionType>): IDispatchProps {
    return {
        actions: new Actions(dispatch)
    };
}

const connectCompanies = connect(
    mapStateToProps,
    mapDispatchToProps
)(Companies);

export {connectCompanies as Companies};
