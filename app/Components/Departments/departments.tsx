import * as React from 'react';
import {connect} from 'react-redux';
import {match} from 'react-router';
import {Link} from 'react-router-dom';
import {Dispatch} from 'redux';
import {
    IActionType,
    ICommonConditions,
    ICompanies,
    IDepartmentsItem,
    IDispatchProps,
    IModalLabels
} from '../../common';
import {Actions} from '../../Actions/actions';

import {Modal} from '../Modal/modal';

/** Интерфейс для IStateProps */
interface IDetailParams {
    companyId: string;
}

/** Интерфейс массива отделов*/
interface IStateProps {
    departments: IDepartmentsItem[];
    match?: match<IDetailParams>;
}

/** Итоговый интерфейс для props */
type TProps = IStateProps & IDispatchProps & ICommonConditions;

/** Интерфейс для объекта-отдела*/
interface IInput {
    name: string;
    phone: string;
}

/** Интерфейс объекта-отдела*/
interface IDepartmentItem {
    item: IInput;
}

/** Итоговый интерфейс для state */
type TState = IDepartmentItem & IModalLabels;

/** Компонент отвечающий за отделы*/
class Departments extends React.Component<TProps, TState> {

    /** Стейт компонента
     * @confirmName (string) - название кнопки подтверждения действия
     * в модальном окне
     * @title (string) - заголовок модального окна
     * @currentID (number) - ID текущего отдела
     * @item {} - Объект с полями name, address, INN организации
     * @item.name (string) - название отдела
     * @item.phone (string) - телефон отдела */
    state: TState = {
        confirmName: '',
        title: '',
        currentID: null,
        item: {
            name: '',
            phone: '',
        }
    };

    /** Получение списка отделов*/
    componentDidMount() {
        const {
            match: {params: {companyId: id}}
        } = this.props;
        this.setState({
            currentID: parseInt(id)
        });
        this.props.actions.getDepartments(id);
    }

    /** Очистка стейта*/
    cleanState = () => {
        this.setState({
            confirmName: '',
            title: '',
            item: {
                name: '',
                phone: '',
            }
        });
    };

    /** Изменение данных в модальном окне
     * @e (SyntheticEvent) - оболочка события для измнения полей input*/
    onChange = (e: React.SyntheticEvent<HTMLInputElement>) => {
        const target = e.currentTarget;

        if (target.id === 'phone') {
            const charCode = target.value.charCodeAt(target.value.length - 1);
            if ((charCode >= 48 && charCode <= 57) || charCode === 46 || charCode === 8) {
                this.setState({
                    item: {
                        ...this.state.item,
                        [target.id]: target.value
                    }
                });
            }
        } else {
            const target = e.currentTarget;
            this.setState({
                item: {
                    ...this.state.item,
                    [target.id]: target.value
                }
            });
        }
    };

    /** Поиск отдела
     * @idValue (number) - значение ID искомого отдела*/
    searchDepartment(idValue: number): IDepartmentsItem {
        const departments = this.props.departments;
        let item: IDepartmentsItem;

        for (let i = 0; i < departments.length; i++) {
            if (departments[i].id === idValue) {
                item = departments[i];
                return item;
            }
        }
        return
    };

    /** Заглушка для запрета изменения полей input при удалении*/
    nothing = () => {
        console.log('Absolutely nothing is happening')
    };

    /** Обработчик кнопки "Добавить организацию" */
    handleAdd = () => {
        this.props.actions.switchAddModal();
        this.setState({
            confirmName: 'ДОБАВИТЬ',
            title: 'Добавление отдела',
        });
    };

    /** Обработчик кнопки "РЕДАКТИРОВАТЬ"
     * @e (SyntheticEvent) - оболочка события для клика
     * на кнопке Edit модального окна*/
    handleEdit = (e: React.SyntheticEvent<HTMLElement>) => {
        this.props.actions.switchEditModal();
        const idValue = parseInt(e.currentTarget.dataset.id);
        const department = this.searchDepartment(idValue);
        this.setState({
            confirmName: 'СОХРАНИТЬ',
            title: 'Редактирование отдела',
            currentID: idValue,
            item: {
                name: department.name,
                phone: department.phone
            }
        });
    };

    /** Обработчик кнопки "УДАЛИТЬ"
     * @e (SyntheticEvent) - оболочка события для клика
     * на кнопке Delete модального окна*/
    handleDelete = (e: React.SyntheticEvent<HTMLElement>) => {
        this.props.actions.switchDeleteModal();
        const idValue = parseInt(e.currentTarget.dataset.id);
        const department = this.searchDepartment(idValue);

        this.setState({
            confirmName: 'УДАЛИТЬ',
            title: 'Удаление отдела',
            currentID: idValue,
            item: {
                name: department.name,
                phone: department.phone
            }
        });
    };

    /** Обработчик кнопки "К СОТРУДНИКАМ"
     * @e (SyntheticEvent) - оболочка события для клика
     * на кнопке To Employees модального окна*/
    handleToEmployees = (e: React.SyntheticEvent<HTMLElement>) => {
        const orgID = parseInt(e.currentTarget.dataset.orgid)
        this.props.actions.putID(orgID);
    };

    /** Обработчик кнопки close модального окна Add*/
    handleCloseAdd = () => {
        this.props.actions.switchAddModal();
        this.cleanState();
    };

    /** Обработчик кнопки close модального окна Edit*/
    handleCloseEdit = () => {
        this.props.actions.switchEditModal();
        this.cleanState();
    };

    /** Обработчик кнопки close модального окна Delete*/
    handleCloseDelete = () => {
        this.props.actions.switchDeleteModal();
        this.cleanState();
    };

    /** Обработчик кнопки "Add department" модального окна Add */
    handleAddConfirm = () => {
        this.props.actions.addDepartment({
            id_organization: this.state.currentID,
            ...this.state.item,
        }, this.props.departments);
        this.cleanState();
    };

    /** Обработчик кнопки "Save changes" модального окна Edit*/
    handleEditConfirm = () => {
        this.props.actions.editDepartment({
            ...this.state.item,
            id: this.state.currentID,
        }, this.props.departments);
        this.cleanState();
    };

    /** Обработчик кнопки "Delete" модального окна Delete*/
    handleDeleteConfirm = () => {
        this.props.actions.deleteDepartment(this.state.currentID, this.props.departments);
        this.cleanState();
    };

    /** Блок отделов*/
    nonModal() {
        let {departments, loginStatus} = this.props;
        /** Вход выполнен */
        if (loginStatus) {
            /** Вход выполнен, организаций нет */
            if (departments.length === 0) {
                return (
                    <React.Fragment>
                        <div className="vIndent5"></div>
                        <div
                            className="btn-primary txtCenter loginBtn"
                            onClick={this.handleAdd}>
                            Добавить отдел
                        </div>
                    </React.Fragment>
                )
            }
            /** Вход выполнен, организации есть */
            else {
                let output: JSX.Element | JSX.Element[] = departments.map(item => {
                    return (
                        <React.Fragment>
                            <div className="vIndent2_5"></div>
                            <div key={item.id}>
                                <div>
                                    <div>
                                        <div
                                            className="lightBack btn-no-hover txtCenter loginBtn">
                                            Название {item.name}
                                        </div>
                                        <div
                                            className="lightBack btn-no-hover txtCenter loginBtn">
                                            Телефон {item.phone}
                                        </div>
                                    </div>
                                    <div className="row pads">
                                        <div
                                            className="lightBack btn-primary width33 txtCenter loginBtn"
                                            data-id={item.id.toString()}
                                            onClick={this.handleEdit}>
                                            РЕДАКТИРОВАТЬ
                                        </div>
                                        <div
                                            className="lightBack btn-primary width29 txtCenter loginBtn"
                                            data-id={item.id.toString()}
                                            onClick={this.handleDelete}>
                                            УДАЛИТЬ
                                        </div>
                                        <Link
                                            onClick={this.handleToEmployees}
                                            data-orgId={item.id_organization}
                                            className="lightBack btn-primary width33 txtCenter loginBtn"
                                            to={`/employee/${item.id}`}>
                                            К СОТРУДНИКАМ
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        </React.Fragment>
                    );
                });
                return (
                    <React.Fragment>
                        <div className="vIndent5"></div>
                        <div
                            className="btn-primary txtCenter loginBtn"
                            onClick={this.handleAdd}>
                            Добавить отдел
                        </div>
                        {output}
                    </React.Fragment>
                );
            }
        }
        /** Вход не выполнен */
        else {
            return (
                <div>
                    Пожалуйста залогиньтесь
                </div>
            )
        }
    };

    /** Модальные окна*/
    modal() {
        /** для добавления*/
        if (this.props.modalAdd) {
            return <Modal
                confirmName={this.state.confirmName}
                title={this.state.title}
                isOpen={this.props.modalAdd}
                close={this.handleCloseAdd}
                confirm={this.handleAddConfirm}
                onChange={this.onChange}
                data={this.state.item}/>
        }
        /** для редактирования*/
        if (this.props.modalEdit) {
            return <Modal
                confirmName={this.state.confirmName}
                title={this.state.title}
                isOpen={this.props.modalEdit}
                close={this.handleCloseEdit}
                confirm={this.handleEditConfirm}
                onChange={this.onChange}
                data={this.state.item}/>
        }
        /** для удаления */
        if (this.props.modalDelete) {
            return <Modal
                confirmName={this.state.confirmName}
                title={this.state.title}
                isOpen={this.props.modalDelete}
                close={this.handleCloseDelete}
                confirm={this.handleDeleteConfirm}
                onChange={this.nothing}
                data={this.state.item}/>
        }
        return
    }

    /** Орисовка блока отделов и модальных окон*/
    render() {
        return (
            <React.Fragment>
                <div>{this.nonModal()}</div>
                <div>{this.modal()}</div>
            </React.Fragment>
        )

    }
}

function mapStateToProps(state: ICompanies) {
    return {...state.companiesRdc, ...state.loginRdc};
}

function mapDispatchToProps(dispatch: Dispatch<IActionType>): IDispatchProps {
    return {
        actions: new Actions(dispatch)
    };
}

const connectCompanies = connect(
    mapStateToProps,
    mapDispatchToProps
)(Departments);

export {connectCompanies as Departments};
