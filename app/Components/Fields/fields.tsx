import * as React from 'react';
import {IInput} from '../../common'

/** Компонент отвечающий за поля ввода*/
export const Fields = (props: any) => {

    const {item, onChange} = props;

    const inputField: IInput = {
        name: 'Название',
        address: 'Адрес',
        INN: 'ИНН',
        phone: 'Телефон',
        position: 'Должность',
        FIO: 'Ф.И.О'
    };

    return (
        <div>
            {Object.keys(item).map((key: string) => {
                let value: string;
                value = item[key];

                //@ts-ignore
                let label = inputField[key];
                console.log(typeof label)

                return (
                    <div key={key} className="container">
                        <div className="row">

                            <label className="col-4 fsize">{label}</label>
                            <input
                                className="col-8"
                                id={key}
                                value={value}
                                onChange={onChange}
                            />
                        </div>
                        <br/>
                    </div>
                );
            })}
        </div>
    );
};
