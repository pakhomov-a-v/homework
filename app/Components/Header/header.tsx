import * as React from 'react';
import {Link} from 'react-router-dom';

/** Компонент отвечающий за шапку*/
const Header = () => {
    return (
        <div>
            <Link className="btn btn-primary marginsLR" to="/">Задание</Link>
            <Link className="btn btn-primary marginsLR" to="/login">Авторизация</Link>
            <Link className="btn btn-primary marginsLR" to="/companies">Организации</Link>
        </div>
    );
};

export default Header;
