import * as React from 'react';
import {connect} from 'react-redux';
import {Link, match} from 'react-router-dom';
import {Dispatch} from 'redux';
import {
    IActionType,
    ICommonConditions,
    ICompanies,
    IDispatchProps,
    IEmployeesItem,
    IModalLabels
} from '../../common';
import {Actions} from '../../Actions/actions';

import {Modal} from '../Modal/modal'

/** Интерфейс для IStateProps */
interface IDetailParams {
    divisionId: string;
}

/** Интерфейс массива сотрудников*/
interface IStateProps {
    employees: IEmployeesItem[];
    match?: match<IDetailParams>;
}

/** Итоговый интерфейс для props */
type TProps = IStateProps & IDispatchProps & ICommonConditions;

/** Интерфейс для объекта-сотрудника*/
interface IInput {
    FIO: string;
    address: string;
    position: string;
}

/** Интерфейс объекта-сотрудника*/
interface IEmployeeItem {
    item: IInput;
}

/** Итоговый интерфейс для state */
type TState = IEmployeeItem & IModalLabels;

/** Компонент отвечающий за сотрудников*/
class Employees extends React.Component<TProps, TState> {

    /** Стейт компонента
     * @confirmName (string) - название кнопки подтверждения действия
     * в модальном окне
     * @title (string) - заголовок модального окна
     * @currentID (number) - ID текущго сотрудника
     * @item {} - Объект с полями FIO, address, position сотрудника
     * @item.FIO (string) - имя сотрудника
     * @item.address (string) - адрес сотрудника
     * @item.position (string) - должность сотрудника */
    state: TState = {
        confirmName: '',
        title: '',
        currentID: null,
        item: {
            FIO: '',
            address: '',
            position: ''
        }
    };

    /** Получение списка сотрудников*/
    componentDidMount() {
        const {
            match: {params: {divisionId: id}}
        } = this.props;
        this.setState({
            currentID: parseInt(id)
        });
        this.props.actions.getEmployees(id);
    }

    /** Очистка стейта*/
    cleanState = () => {
        this.setState({
            confirmName: '',
            title: '',
            item: {
                FIO: '',
                address: '',
                position: ''
            }
        });
    };

    /** Изменение данных в модальном окне
     * @e (SyntheticEvent) - оболочка события для измнения полей input*/
    onChange = (e: React.SyntheticEvent<HTMLInputElement>) => {
        const target = e.currentTarget;
        this.setState({
            item: {
                ...this.state.item,
                [target.id]: target.value
            }
        });
    };

    /** Поиск сотрудника
     * @idValue (number) - значение ID искомого  сотрудника*/
    searchEmployee(idValue: number): IEmployeesItem {
        const employees = this.props.employees;
        let item: IEmployeesItem;

        for (let i = 0; i < employees.length; i++) {
            if (employees[i].id === idValue) {
                item = employees[i];
                return item;
            }
        }
        return
    };

    /** Заглушка для запрета изменения полей input при удалении*/
    nothing = () => {
        console.log('Absolutely nothing is happening')
    };

    /** Обработчик кнопки "Добавить сотрудника" */
    handleAdd = () => {
        this.props.actions.switchAddModal();
        this.setState({
            confirmName: 'ДОБАВИТЬ',
            title: 'Добавление сотрудника',
        });
    };

    /** Обработчик кнопки "РЕДАКТИРОВАТЬ"
     * @e (SyntheticEvent) - оболочка события для клика
     * на кнопке Edit модального окна*/
    handleEdit = (e: React.SyntheticEvent<HTMLElement>) => {
        this.props.actions.switchEditModal();
        const idValue = parseInt(e.currentTarget.dataset.id);
        const employee = this.searchEmployee(idValue);

        this.setState({
            confirmName: 'СОХРАНИТЬ',
            title: 'Редактирование сотрудника',
            currentID: idValue,
            item: {
                FIO: employee.FIO,
                address: employee.address,
                position: employee.position,
            }
        });
    };

    /** Обработчик кнопки "УДАЛИТЬ"
     * @e (SyntheticEvent) - оболочка события для клика
     * на кнопке Delete модального окна*/
    handleDelete = (e: React.SyntheticEvent<HTMLElement>) => {
        this.props.actions.switchDeleteModal();
        const idValue = parseInt(e.currentTarget.dataset.id);
        const employee = this.searchEmployee(idValue);

        this.setState({
            confirmName: 'УДАЛИТЬ',
            title: 'Удаление сотрудника',
            currentID: idValue,
            item: {
                FIO: employee.FIO,
                address: employee.address,
                position: employee.position,
            }
        });
    };

    /** Обработчик кнопки close модального окна Add*/
    handleCloseAdd = () => {
        this.props.actions.switchAddModal();
        this.cleanState();
    };

    /** Обработчик кнопки close модального окна Edit*/
    handleCloseEdit = () => {
        this.props.actions.switchEditModal();
        this.cleanState();
    };

    /** Обработчик кнопки close модального окна Delete*/
    handleCloseDelete = () => {
        this.props.actions.switchDeleteModal();
        this.cleanState();
    };

    /** Обработчик кнопки "Add employee" модального окна Add */
    handleAddConfirm = () => {
        this.props.actions.addEmployee({
            ...this.state.item,
            id_division: this.state.currentID
        }, this.props.employees);
        this.cleanState();
    };

    /** Обработчик кнопки "Save changes" модального окна Edit*/
    handleEditConfirm = () => {
        this.props.actions.editEmployee({
            ...this.state.item,
            id: this.state.currentID,
        }, this.props.employees);
        this.cleanState();
    };

    /** Обработчик кнопки "Delete" модального окна Delete*/
    handleDeleteConfirm = () => {
        this.props.actions.deleteEmployee(this.state.currentID, this.props.employees);
        this.cleanState();
    };

    /** Блок сотрудников*/
    nonModal() {
        let {employees, loginStatus} = this.props;
        /** Вход выполнен */
        if (loginStatus) {
            /** Вход выполнен, организаций нет */
            if (employees.length === 0) {
                return (
                    <React.Fragment>
                        <div className="vIndent5"></div>
                        <div
                            className="btn-primary txtCenter loginBtn"
                            onClick={this.handleAdd}>
                            Добавить сотрудника
                        </div>
                    </React.Fragment>
                )
            }
            /** Вход выполнен, организации есть */
            else {
                let output: JSX.Element | JSX.Element[] | string =
                    employees.map(item => {
                        return (
                            <React.Fragment>
                                <div className="vIndent2_5"></div>
                                <div key={item.id}>
                                    <div>
                                        <div>
                                            <div
                                                className="lightBack btn-no-hover txtCenter loginBtn">
                                                ФИО {item.FIO}
                                            </div>
                                            <div
                                                className="lightBack btn-no-hover txtCenter loginBtn">
                                                Адрес {item.address}
                                            </div>
                                            <div
                                                className="lightBack btn-no-hover txtCenter loginBtn">
                                                Должность {item.position}
                                            </div>
                                        </div>
                                        <div className="row pads">
                                            <div
                                                className="lightBack btn-primary width33 txtCenter loginBtn"
                                                data-id={item.id.toString()}
                                                onClick={this.handleEdit}>
                                                РЕДАКТИРОВАТЬ
                                            </div>
                                            <div
                                                className="lightBack btn-primary width29 txtCenter loginBtn"
                                                data-id={item.id.toString()}
                                                onClick={this.handleDelete}>
                                                УДАЛИТЬ
                                            </div>
                                            <Link
                                                className="lightBack btn-primary width33 txtCenter loginBtn"
                                                to={`/department/${this.props.orgId}`}>
                                                К ОТДЕЛАМ
                                            </Link>
                                        </div>
                                    </div>
                                </div>
                            </React.Fragment>
                        );
                    });
                return (
                    <React.Fragment>
                        <div className="vIndent5"></div>
                        <div
                            className="btn-primary txtCenter loginBtn"
                            onClick={this.handleAdd}>
                            Добавить сотрудника
                        </div>
                        {output}
                    </React.Fragment>
                );
            }
        }
        /** Вход не выполнен */
        else {
            return (
                <div>
                    Пожалуйста залогиньтесь
                </div>
            )
        }
    };

    /** Модальные окна*/
    modal() {
        /** для добавления*/
        if (this.props.modalAdd) {
            return <Modal
                confirmName={this.state.confirmName}
                title={this.state.title}
                isOpen={this.props.modalAdd}
                close={this.handleCloseAdd}
                confirm={this.handleAddConfirm}
                onChange={this.onChange}
                data={this.state.item}/>
        }
        /** для редактирования*/
        if (this.props.modalEdit) {
            return <Modal
                confirmName={this.state.confirmName}
                title={this.state.title}
                isOpen={this.props.modalEdit}
                close={this.handleCloseEdit}
                confirm={this.handleEditConfirm}
                onChange={this.onChange}
                data={this.state.item}/>
        }
        /** для удаления*/
        if (this.props.modalDelete) {
            return <Modal
                confirmName={this.state.confirmName}
                title={this.state.title}
                isOpen={this.props.modalDelete}
                close={this.handleCloseDelete}
                confirm={this.handleDeleteConfirm}
                onChange={this.nothing}
                data={this.state.item}/>
        }
        return
    }

    /** Орисовка блока сотрудников и модальных окон*/
    render() {
        return (
            <React.Fragment>
                <div>{this.nonModal()}</div>
                <div>{this.modal()}</div>
            </React.Fragment>
        )
    }
}

function mapStateToProps(state: ICompanies) {
    return {...state.companiesRdc, ...state.loginRdc};
}

function mapDispatchToProps(dispatch: Dispatch<IActionType>): IDispatchProps {
    return {
        actions: new Actions(dispatch)
    };
}

const connectEmployees = connect(
    mapStateToProps,
    mapDispatchToProps
)(Employees);

export {connectEmployees as Employees};
