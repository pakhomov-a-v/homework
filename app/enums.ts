/** Общие типы*/
export enum CommonTypes {
    LOGIN = 'ACTION_LOGIN',
    LOGOUT = 'ACTION_LOGOUT',
    COMPANIES = 'ORGANIZATION',
    DEPARTMENTS = 'DEPARTMENTS',
    EMPLOYEES = 'EMPLOYEES',
    MODAL = 'MODAL_',
}

/** Типы асинхронных действий*/
export enum AsyncActionTypes {
    BEGIN = '_BEGIN',
    SUCCESS = '_SUCCESS',
    FAILURE = '_FAILURE'
}

/** Типы CRUD и переключатель*/
export enum CRUDTypes {
    CREATE = 'CREATE_',
    DELETE = 'DELETE_',
    UPDATE = 'UPDATE',
    GET = 'GET_',
    SWITCH = 'SWITCH'
}
