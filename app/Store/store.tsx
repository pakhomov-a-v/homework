import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import {rootReducer} from '../Reducers/combined';

/** Store прложения*/
export const store = createStore(rootReducer, applyMiddleware(thunk));
