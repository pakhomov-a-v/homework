import {IActionType, ICompaniesItem, IDepartmentsItem, IEmployeesItem} from '../common';
import {AsyncActionTypes, CommonTypes, CRUDTypes} from '../enums'

/** Массив компаний*/
const companiesArray: ICompaniesItem[] = [];
/** Массив отделов*/
const departmentsArray: IDepartmentsItem[] = [];
/** Массив сотрудников*/
const employeesArray: IEmployeesItem[] = [];

/** Начальное стостояние state
 * @companies (ICompaniesItem[]) - массив компаний
 * @departments (IDepartmentsItem[]) - массив отделов
 * @employees (IEmployeesItem[]) - массив сотрудников
 * @loading (boolean) - состояние загрузки
 * @modalAdd (boolean) - открыто-ли модальное окно Add
 * @modalDelete (boolean) - открыто-ли модальное окно Delete
 * @modalEdit (boolean) - открыто-ли модальное окно Edit*/
const initialState = {
    companies: companiesArray,
    departments: departmentsArray,
    employees: employeesArray,
    loading: false,
    modalAdd: false,
    modalDelete: false,
    modalEdit: false,
    orgId: 0
};

/** Основной редьюсер*/
export function companiesReducer(state = initialState, action: IActionType) {

    switch (action.type) {
        /** Изменения стора при получении данных об организациях
         (возможные состояния - загрузка, успех, отказ)*/
        case `${CRUDTypes.GET}${CommonTypes.COMPANIES}${AsyncActionTypes.BEGIN}`:
            return {
                ...state,
                loading: true
            };

        case `${CRUDTypes.GET}${CommonTypes.COMPANIES}${AsyncActionTypes.SUCCESS}`:
            return {
                ...state,
                companies: action.payload,
                loading: false
            };

        case `${CRUDTypes.GET}${CommonTypes.COMPANIES}${AsyncActionTypes.FAILURE}`:
            return {
                ...state,
                loading: false
            };

        /** Изменения стора при получении данных об отделах
         (возможные состояния - загрузка, успех, отказ)*/
        case `${CRUDTypes.GET}${CommonTypes.DEPARTMENTS}${AsyncActionTypes.BEGIN}`:
            return {
                ...state,
                loading: true
            };

        case `${CRUDTypes.GET}${CommonTypes.DEPARTMENTS}${AsyncActionTypes.SUCCESS}`:
            return {
                ...state,
                departments: action.payload,
                loading: false
            };

        case `${CRUDTypes.GET}${CommonTypes.DEPARTMENTS}${AsyncActionTypes.FAILURE}`:
            return {
                ...state,
                loading: false
            };

        /** Изменения стора при получении данных о сотрудниках
         (возможные состояния - загрузка, успех, отказ)*/
        case `${CRUDTypes.GET}${CommonTypes.EMPLOYEES}${AsyncActionTypes.BEGIN}`:
            return {
                ...state,
                loading: true
            };

        case `${CRUDTypes.GET}${CommonTypes.EMPLOYEES}${AsyncActionTypes.SUCCESS}`:
            return {
                ...state,
                employees: action.payload,
                loading: false
            };

        case `${CRUDTypes.GET}${CommonTypes.EMPLOYEES}${AsyncActionTypes.FAILURE}`:
            return {
                ...state,
                loading: false
            };

        /** Добавление компаний, организаций, сотрудников*/
        case `${CRUDTypes.CREATE}${CommonTypes.COMPANIES}`:
            return {
                ...state,
                companies: action.payload,
            };

        case `${CRUDTypes.CREATE}${CommonTypes.DEPARTMENTS}`:
            return {
                ...state,
                departments: action.payload,
            };

        case `${CRUDTypes.CREATE}${CommonTypes.EMPLOYEES}`:
            return {
                ...state,
                employees: action.payload,
            };

        /** Редактирование компаний, организаций, сотрудников*/
        case `${CRUDTypes.UPDATE}${CommonTypes.COMPANIES}`:
            return {
                ...state,
                companies: action.payload,
            };

        case `${CRUDTypes.UPDATE}${CommonTypes.DEPARTMENTS}`:
            return {
                ...state,
                departments: action.payload
            };

        case `${CRUDTypes.UPDATE}${CommonTypes.EMPLOYEES}`:
            return {
                ...state,
                employees: action.payload
            };

        /** Удаление компаний, организаций, сотрудников*/
        case `${CRUDTypes.DELETE}${CommonTypes.COMPANIES}`:
            const invertDeleteCompany = !state.modalDelete;
            return {
                ...state,
                companies: action.payload,
                modalDelete: invertDeleteCompany
            };

        case `${CRUDTypes.DELETE}${CommonTypes.DEPARTMENTS}`:
            const invertDeleteDepartment = !state.modalDelete;
            return {
                ...state,
                departments: action.payload,
                modalDelete: invertDeleteDepartment
            };

        case `${CRUDTypes.DELETE}${CommonTypes.EMPLOYEES}`:
            const invertDeleteEmployee = !state.modalDelete;
            return {
                ...state,
                employees: action.payload,
                modalDelete: invertDeleteEmployee
            };

        /** Переключение модальных окон добаления и редактирования*/
        case `${CommonTypes.MODAL}${CRUDTypes.CREATE}${CRUDTypes.SWITCH}`:
            const invertAdd = !state.modalAdd;
            return {
                ...state,
                modalAdd: invertAdd
            };

        case `${CommonTypes.MODAL}${CRUDTypes.UPDATE}${CRUDTypes.SWITCH}`:
            const invertEdit = !state.modalEdit;
            return {
                ...state,
                modalEdit: invertEdit
            };

        case `${CommonTypes.MODAL}${CRUDTypes.DELETE}${CRUDTypes.SWITCH}`:
            const invertDelete = !state.modalDelete;
            return {
                ...state,
                modalDelete: invertDelete
            };

        /** Сохранение ID организации для кнопки To Departments на странице сотрудников*/
        case 'ORG_ID':
            return {
                ...state,
                orgId: action.payload
            };

        default:
            return state
    }
}
