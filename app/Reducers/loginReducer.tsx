import {IActionType} from '../common';
import {AsyncActionTypes, CommonTypes} from '../enums';

/** Интерфейс для state*/
interface IStoreState {
    loginStatus: boolean;
    loginLoading: boolean;
}

/** Начальное стостояние state
 * @loginStatus (boolean) - состояние авторизации
 * @loginLoading (boolean) - состояние загрузки*/
const initialState: IStoreState = {
    loginStatus: false,
    loginLoading: false
};

/** Редьюсер логина*/
export function loginReducer(state = initialState, action: IActionType) {
    switch (action.type) {
        /** Загрузка*/
        case `${CommonTypes.LOGIN}${AsyncActionTypes.BEGIN}`:
            return {
                ...state,
                loginLoading: true
            };
        /** Авторизация успешна*/
        case `${CommonTypes.LOGIN}${AsyncActionTypes.SUCCESS}`:
            return {
                ...state,
                loginStatus: true,
                loginLoading: false
            };
        /** Авторизация не выполнена*/
        case `${CommonTypes.LOGIN}${AsyncActionTypes.FAILURE}`:
            return {
                ...state,
                loginLoading: false,
                loginStatus: false
            };
        /** Соверше выход из БД*/
        case CommonTypes.LOGOUT:
            return {
                ...state,
                loginStatus: false
            };
    }
    return state;
}
