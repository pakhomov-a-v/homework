import {combineReducers} from 'redux';
import {companiesReducer} from './companiesReducer'
import {loginReducer} from './loginReducer';

/** Комбинированый редьюсер*/
export const rootReducer = combineReducers({
    loginRdc: loginReducer,
    companiesRdc: companiesReducer,
});
