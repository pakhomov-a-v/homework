import Axios from 'axios';
import {Dispatch} from 'redux';
import {
    IActionType,
    ICompaniesItem,
    IDepartmentsItem,
    IEmployeesItem,
    ILogin,
} from '../common';
import {AsyncActionTypes, CommonTypes, CRUDTypes} from '../enums';

/** Actions приложени*/
export class Actions {
    constructor(private dispatch: Dispatch<IActionType>) {
    }

    /** Action-creator для входа в БД
     * @loginData (ILogin) логин и пароль пользователя*/
    onLogin = (loginData: ILogin) => {
        this.dispatch({type: `${CommonTypes.LOGIN}${AsyncActionTypes.BEGIN}`});
        Axios.post(`http://localhost:8080/athorize`, {loginData})
            .then(res => {
                if (res.data.isLogin) {
                    this.dispatch({
                        type: `${CommonTypes.LOGIN}${AsyncActionTypes.SUCCESS}`
                    });
                    console.log('Login complete');
                } else {
                    throw 'Something wrong with login';
                }
            })
            .catch(error => {
                this.dispatch({
                    type: `${CommonTypes.LOGIN}${AsyncActionTypes.FAILURE}`,
                    payload: error
                });
            });
    };

    /** Action-creator для выхода из БД*/
    onLogout = () => {
        this.dispatch({type: CommonTypes.LOGOUT});
        console.log('Logout complete');
    };

    /** Action-creator для получения списка организаций*/
    getCompanies = () => {
        this.dispatch({
            type: `${CRUDTypes.GET}${CommonTypes.COMPANIES}${
                AsyncActionTypes.BEGIN}`
        });
        Axios.get(`http://localhost:8080/organization`)
            .then(res => {
                if (res.data) {
                    this.dispatch({
                        type: `${CRUDTypes.GET}${CommonTypes.COMPANIES}${
                            AsyncActionTypes.SUCCESS}`,
                        payload: res.data
                    });
                    console.log('Companies data acquisition completed');
                } else {
                    throw 'Something wrong with companies data acquisition';
                }
            })
            .catch(error => {
                console.log(error);
                this.dispatch({
                    type: `${CRUDTypes.GET}${CommonTypes.COMPANIES}${
                        AsyncActionTypes.FAILURE}`,
                    payload: error
                });
            });
    };

    /** Action-creator для получения списка отделов
     * @id (string) ID организации, список отделов которой хотим получить*/
    getDepartments = (id: string) => {
        this.dispatch({
            type: `${CRUDTypes.GET}${CommonTypes.DEPARTMENTS}${
                AsyncActionTypes.BEGIN}`
        });
        Axios.get(`http://localhost:8080/division?id=${id}`)
            .then(res => {
                if (res.data) {
                    this.dispatch({
                        type: `${CRUDTypes.GET}${CommonTypes.DEPARTMENTS}${
                            AsyncActionTypes.SUCCESS}`,
                        payload: res.data
                    });
                    console.log('Departments data acquisition completed');
                } else {
                    throw 'Something wrong with departments data acquisition';
                }
            })
            .catch(error => {
                this.dispatch({
                    type: `${CRUDTypes.GET}${CommonTypes.DEPARTMENTS}${
                        AsyncActionTypes.FAILURE}`,
                    payload: error
                });
            });
    };

    /** Action-creator для получения списка сотрудников
     * @id (string) ID отдела, список сотрудников которго хотим получить*/
    getEmployees = (id: string) => {
        this.dispatch({
            type: `${CRUDTypes.GET}${CommonTypes.EMPLOYEES}${
                AsyncActionTypes.BEGIN}`
        });
        Axios.get(`http://localhost:8080/employee?id=${id}`)
            .then(res => {
                if (res.data) {
                    this.dispatch({
                        type: `${CRUDTypes.GET}${CommonTypes.EMPLOYEES}${
                            AsyncActionTypes.SUCCESS}`,
                        payload: res.data
                    });
                    console.log('Employees data acquisition completed');
                } else {
                    throw 'Something wrong with employees data acquisition';
                }
            })
            .catch(error => {
                this.dispatch({
                    type: `${CRUDTypes.GET}${CommonTypes.EMPLOYEES}${
                        AsyncActionTypes.FAILURE}`,
                    payload: error
                });
            });
    };

    /** Action-creator для вкл/выкл модального окна добавления*/
    switchAddModal = () => {
        this.dispatch({
            type: `${CommonTypes.MODAL}${CRUDTypes.CREATE}${
                CRUDTypes.SWITCH}`
        });
    };

    /** Action-creator для вкл/выкл модального окна редактировани*/
    switchEditModal = () => {
        this.dispatch({
            type: `${CommonTypes.MODAL}${CRUDTypes.UPDATE}${CRUDTypes.SWITCH}`
        });
    };

    /** Action-creator для вкл/выкл модального окна удаления*/
    switchDeleteModal = () => {
        this.dispatch({
            type: `${CommonTypes.MODAL}${CRUDTypes.DELETE}${CRUDTypes.SWITCH}`
        });
    };

    /** Action-creator для добавления организации*/
    addCompany = (data: ICompaniesItem, companies: ICompaniesItem[]) => {
        Axios.post(`http://localhost:8080/createOrganization`, data)
            .then(res => {
                if (res.data) {
                    const addCompanyId = companies.length + 1;
                    const addCompanyItem = {...data, id: addCompanyId};
                    companies.push(addCompanyItem);
                    this.dispatch({
                        type: `${CRUDTypes.CREATE}${CommonTypes.COMPANIES}`,
                        payload: companies
                    });
                    this.dispatch({
                        type: `${CommonTypes.MODAL}${CRUDTypes.CREATE}${
                            CRUDTypes.SWITCH
                            }`
                    });
                    console.log('Adding company completed');
                } else {
                    throw 'Something goes wrong with adding company';
                }
            })
            .catch(error => {
                console.error(error);
            });
    };

    /** Action-creator для добавления отдела*/
    addDepartment = (data: IDepartmentsItem, departments: IDepartmentsItem[]) => {
        Axios.post(`http://localhost:8080/createDivision`, data)
            .then(res => {
                if (res.data) {
                    const addDepartmentId = departments.length + 1;
                    const addDepartmentItem = {...data, id: addDepartmentId};
                    departments.push(addDepartmentItem);
                    this.dispatch({
                        type: `${CRUDTypes.CREATE}${CommonTypes.DEPARTMENTS}`,
                        payload: departments
                    });
                    this.dispatch({
                        type: `${CommonTypes.MODAL}${CRUDTypes.CREATE}${CRUDTypes.SWITCH}`
                    });
                    console.log('Adding department completed');
                } else {
                    throw 'Something goes wrong with adding department';
                }
            })
            .catch(error => {
                console.error(error);
            });
    };

    /** Action-creator для добавления сотрудника*/
    addEmployee = (data: IEmployeesItem, employees: IEmployeesItem[]) => {
        Axios.post(`http://localhost:8080/createEmployee`, data)
            .then(res => {
                const addEmployeeId = employees.length + 1;
                const addEmployeeItem = {...data, id: addEmployeeId};
                employees.push(addEmployeeItem);
                if (res.data) {
                    this.dispatch({
                        type: `${CRUDTypes.CREATE}${CommonTypes.EMPLOYEES}`,
                        payload: employees
                    });
                    this.dispatch({
                        type: `${CommonTypes.MODAL}${CRUDTypes.CREATE}${CRUDTypes.SWITCH}`
                    });
                    console.log('Adding employee completed');
                } else {
                    throw 'Something goes wrong with adding employee';
                }
            })
            .catch(error => {
                console.error(error);
            });
    };

    /** Action-creator для редактирования организации*/
    editCompany = (data: ICompaniesItem, companies: ICompaniesItem[]) => {
        Axios.put(`http://localhost:8080/editOrganization`, data)
            .then(res => {
                if (res.data) {
                    const editCompanies = companies;
                    for (let i = 0; i < companies.length; i++) {
                        if (data.id === companies[i].id) {
                            editCompanies[i] = data;
                        }
                    }
                    this.dispatch({
                        type: `${CRUDTypes.UPDATE}${CommonTypes.COMPANIES}`,
                        payload: editCompanies
                    });
                    this.dispatch({
                        type: `${CommonTypes.MODAL}${CRUDTypes.UPDATE}${CRUDTypes.SWITCH}`
                    });
                    console.log('Editing company completed');
                } else {
                    throw 'Something goes wrong with editing company';
                }
            })
            .catch(error => {
                console.error(error);
            });
    };

    /** Action-creator для редактирования отдела*/
    editDepartment = (data: IDepartmentsItem, departments: IDepartmentsItem[]) => {
        Axios.put(`http://localhost:8080/editDivision`, data)
            .then(res => {
                if (res.data) {
                    let editDepartments = departments;
                    for (let i = 0; i < departments.length; i++) {
                        if (data.id === departments[i].id) {
                            editDepartments[i] = data;
                        }
                    }
                    this.dispatch({
                        type: `${CRUDTypes.UPDATE}${CommonTypes.DEPARTMENTS}`,
                        payload: editDepartments
                    });
                    this.dispatch({
                        type: `${CommonTypes.MODAL}${CRUDTypes.UPDATE}${CRUDTypes.SWITCH}`
                    });
                    console.log('Editing department completed');
                } else {
                    throw 'Something goes wrong with editing department';
                }
            })
            .catch(error => {
                console.error(error);
            });
    };

    /** Action-creator для редактирования сотрудника*/
    editEmployee = (data: IEmployeesItem, employees: IEmployeesItem[]) => {
        Axios.put(`http://localhost:8080/editEmployee`, data)
            .then(res => {
                if (res.data) {
                    let editEmployees = employees;
                    for (let i = 0; i < employees.length; i++) {
                        if (data.id === employees[i].id) {
                            editEmployees[i] = data;
                        }
                    }
                    this.dispatch({
                        type: `${CRUDTypes.UPDATE}${CommonTypes.EMPLOYEES}`,
                        payload: editEmployees
                    });
                    this.dispatch({
                        type: `${CommonTypes.MODAL}${CRUDTypes.UPDATE}${CRUDTypes.SWITCH}`
                    });
                    console.log('Editing employee completed');
                } else {
                    throw 'Something goes wrong with editing employee';
                }
            })
            .catch(error => {
                console.error(error);
            });
    };

    /** Action-creator для удаления организации*/
    deleteCompany = (id: number, companies: ICompaniesItem[]) => {
        Axios.delete(`http://localhost:8080/deleteOrganization?id=${id}`)
            .then(res => {
                if (res.data) {
                    let companyIndex: number;
                    for (let i = 0; i < companies.length; i++) {
                        if (id === companies[i].id) {
                            companyIndex = i;
                        }
                    }
                    const deleteCompanies = [
                        ...companies.slice(0, companyIndex),
                        ...companies.slice(companyIndex + 1)
                    ];
                    this.dispatch({
                        type: `${CRUDTypes.DELETE}${CommonTypes.COMPANIES}`,
                        payload: deleteCompanies
                    });
                    console.log('Deleting company completed');
                } else {
                    throw 'Something goes wrong with deleting company';
                }
            })
            .catch(error => {
                console.error(error);
            });
    };

    /** Action-creator для удаления отдела*/
    deleteDepartment = (id: number, departments: IDepartmentsItem[]) => {
        Axios.delete(`http://localhost:8080/deleteDivision?id=${id}`)
            .then(res => {
                if (res.data) {
                    let departmentIndex: number;
                    for (let i = 0; i < departments.length; i++) {
                        if (id === departments[i].id) {
                            departmentIndex = i;
                        }
                    }
                    const deleteDepartments = [
                        ...departments.slice(0, departmentIndex),
                        ...departments.slice(departmentIndex + 1)
                    ];
                    this.dispatch({
                        type: `${CRUDTypes.DELETE}${CommonTypes.DEPARTMENTS}`,
                        payload: deleteDepartments
                    });
                    console.log('Deleting department completed');
                } else {
                    throw 'Something goes wrong with deleting department';
                }
            })
            .catch(error => {
                console.error(error);
            });
    };

    /** Action-creator для удаления сотрудника*/
    deleteEmployee = (id: number, employees: IEmployeesItem[]) => {
        Axios.delete(`http://localhost:8080/deleteEmployee?id=${id}`)
            .then(res => {
                if (res.data) {
                    let employeeIndex: number;
                    for (let i = 0; i < employees.length; i++) {
                        if (id === employees[i].id) {
                            employeeIndex = i;
                        }
                    }
                    const deleteEmployees = [
                        ...employees.slice(0, employeeIndex),
                        ...employees.slice(employeeIndex + 1)
                    ];
                    this.dispatch({
                        type: `${CRUDTypes.DELETE}${CommonTypes.EMPLOYEES}`,
                        payload: deleteEmployees
                    });
                    console.log('Deleting employee completed');
                } else {
                    throw 'Something goes wrong with deleting employee';
                }
            })
            .catch(error => {
                console.error(error);
            });
    };

    /** Action-creator для сохранения ID организации*/
    putID = (orgId: number) =>{
        this.dispatch({
            type: 'ORG_ID',
            payload: orgId
        });
    }
}
